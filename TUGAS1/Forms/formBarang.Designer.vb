﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.etStok = New System.Windows.Forms.TextBox()
        Me.etHargaBarang = New System.Windows.Forms.TextBox()
        Me.etNamaBarang = New System.Windows.Forms.TextBox()
        Me.etKdBarang = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.btHapus = New System.Windows.Forms.Button()
        Me.btUbah = New System.Windows.Forms.Button()
        Me.btTambah = New System.Windows.Forms.Button()
        Me.GridViewTampil = New System.Windows.Forms.DataGridView()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.etCari = New System.Windows.Forms.TextBox()
        Me.btCari = New System.Windows.Forms.Button()
        Me.btLast = New System.Windows.Forms.Button()
        Me.btFirst = New System.Windows.Forms.Button()
        Me.btPrev = New System.Windows.Forms.Button()
        Me.btNext = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        CType(Me.GridViewTampil, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.GroupBox3.SuspendLayout()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.etStok)
        Me.GroupBox1.Controls.Add(Me.etHargaBarang)
        Me.GroupBox1.Controls.Add(Me.etNamaBarang)
        Me.GroupBox1.Controls.Add(Me.etKdBarang)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(26, 51)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(456, 265)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "GroupBox1"
        '
        'etStok
        '
        Me.etStok.Location = New System.Drawing.Point(146, 191)
        Me.etStok.Name = "etStok"
        Me.etStok.Size = New System.Drawing.Size(219, 26)
        Me.etStok.TabIndex = 7
        '
        'etHargaBarang
        '
        Me.etHargaBarang.Location = New System.Drawing.Point(146, 146)
        Me.etHargaBarang.Name = "etHargaBarang"
        Me.etHargaBarang.Size = New System.Drawing.Size(219, 26)
        Me.etHargaBarang.TabIndex = 6
        '
        'etNamaBarang
        '
        Me.etNamaBarang.Location = New System.Drawing.Point(146, 99)
        Me.etNamaBarang.Name = "etNamaBarang"
        Me.etNamaBarang.Size = New System.Drawing.Size(219, 26)
        Me.etNamaBarang.TabIndex = 5
        '
        'etKdBarang
        '
        Me.etKdBarang.Location = New System.Drawing.Point(146, 50)
        Me.etKdBarang.Name = "etKdBarang"
        Me.etKdBarang.Size = New System.Drawing.Size(219, 26)
        Me.etKdBarang.TabIndex = 4
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(8, 197)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(42, 20)
        Me.Label4.TabIndex = 3
        Me.Label4.Text = "Stok"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(8, 146)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(109, 20)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Harga Barang"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(8, 99)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(107, 20)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "Nama Barang"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(8, 50)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(102, 20)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Kode Barang"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btHapus)
        Me.GroupBox2.Controls.Add(Me.btUbah)
        Me.GroupBox2.Controls.Add(Me.btTambah)
        Me.GroupBox2.Location = New System.Drawing.Point(513, 51)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(262, 265)
        Me.GroupBox2.TabIndex = 1
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "GroupBox2"
        '
        'btHapus
        '
        Me.btHapus.Image = CType(resources.GetObject("btHapus.Image"), System.Drawing.Image)
        Me.btHapus.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btHapus.Location = New System.Drawing.Point(28, 169)
        Me.btHapus.Name = "btHapus"
        Me.btHapus.Size = New System.Drawing.Size(113, 48)
        Me.btHapus.TabIndex = 2
        Me.btHapus.Text = "Hapus"
        Me.btHapus.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btHapus.UseVisualStyleBackColor = True
        '
        'btUbah
        '
        Me.btUbah.Image = CType(resources.GetObject("btUbah.Image"), System.Drawing.Image)
        Me.btUbah.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btUbah.Location = New System.Drawing.Point(28, 99)
        Me.btUbah.Name = "btUbah"
        Me.btUbah.Size = New System.Drawing.Size(113, 48)
        Me.btUbah.TabIndex = 1
        Me.btUbah.Text = "Ubah"
        Me.btUbah.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btUbah.UseVisualStyleBackColor = True
        '
        'btTambah
        '
        Me.btTambah.Image = CType(resources.GetObject("btTambah.Image"), System.Drawing.Image)
        Me.btTambah.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btTambah.Location = New System.Drawing.Point(28, 35)
        Me.btTambah.Name = "btTambah"
        Me.btTambah.Size = New System.Drawing.Size(113, 48)
        Me.btTambah.TabIndex = 0
        Me.btTambah.Text = "Tambah"
        Me.btTambah.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btTambah.UseVisualStyleBackColor = True
        '
        'GridViewTampil
        '
        Me.GridViewTampil.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.GridViewTampil.Location = New System.Drawing.Point(26, 322)
        Me.GridViewTampil.Name = "GridViewTampil"
        Me.GridViewTampil.RowTemplate.Height = 28
        Me.GridViewTampil.Size = New System.Drawing.Size(749, 152)
        Me.GridViewTampil.TabIndex = 2
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.etCari)
        Me.GroupBox3.Controls.Add(Me.btCari)
        Me.GroupBox3.Location = New System.Drawing.Point(38, 542)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(444, 76)
        Me.GroupBox3.TabIndex = 3
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Masukkan Data Yang Dicari"
        '
        'etCari
        '
        Me.etCari.Location = New System.Drawing.Point(18, 34)
        Me.etCari.Name = "etCari"
        Me.etCari.Size = New System.Drawing.Size(236, 26)
        Me.etCari.TabIndex = 4
        '
        'btCari
        '
        Me.btCari.Image = CType(resources.GetObject("btCari.Image"), System.Drawing.Image)
        Me.btCari.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btCari.Location = New System.Drawing.Point(278, 25)
        Me.btCari.Name = "btCari"
        Me.btCari.Size = New System.Drawing.Size(75, 45)
        Me.btCari.TabIndex = 4
        Me.btCari.Text = "Cari"
        Me.btCari.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btCari.UseVisualStyleBackColor = True
        '
        'btLast
        '
        Me.btLast.Location = New System.Drawing.Point(700, 576)
        Me.btLast.Name = "btLast"
        Me.btLast.Size = New System.Drawing.Size(75, 39)
        Me.btLast.TabIndex = 4
        Me.btLast.Text = ">>"
        Me.btLast.UseVisualStyleBackColor = True
        '
        'btFirst
        '
        Me.btFirst.Location = New System.Drawing.Point(500, 576)
        Me.btFirst.Name = "btFirst"
        Me.btFirst.Size = New System.Drawing.Size(75, 39)
        Me.btFirst.TabIndex = 5
        Me.btFirst.Text = "<<"
        Me.btFirst.UseVisualStyleBackColor = True
        '
        'btPrev
        '
        Me.btPrev.Location = New System.Drawing.Point(593, 576)
        Me.btPrev.Name = "btPrev"
        Me.btPrev.Size = New System.Drawing.Size(37, 39)
        Me.btPrev.TabIndex = 6
        Me.btPrev.Text = "<"
        Me.btPrev.UseVisualStyleBackColor = True
        '
        'btNext
        '
        Me.btNext.Location = New System.Drawing.Point(646, 576)
        Me.btNext.Name = "btNext"
        Me.btNext.Size = New System.Drawing.Size(37, 39)
        Me.btNext.TabIndex = 7
        Me.btNext.Text = ">"
        Me.btNext.UseVisualStyleBackColor = True
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 646)
        Me.Controls.Add(Me.btNext)
        Me.Controls.Add(Me.btPrev)
        Me.Controls.Add(Me.btFirst)
        Me.Controls.Add(Me.btLast)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GridViewTampil)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.GroupBox1)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        CType(Me.GridViewTampil, System.ComponentModel.ISupportInitialize).EndInit()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents etHargaBarang As TextBox
    Friend WithEvents etNamaBarang As TextBox
    Friend WithEvents etKdBarang As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents etStok As TextBox
    Friend WithEvents btTambah As Button
    Friend WithEvents btHapus As Button
    Friend WithEvents btUbah As Button
    Friend WithEvents GridViewTampil As DataGridView
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents btCari As Button
    Friend WithEvents etCari As TextBox
    Friend WithEvents btLast As Button
    Friend WithEvents btFirst As Button
    Friend WithEvents btPrev As Button
    Friend WithEvents btNext As Button
End Class
