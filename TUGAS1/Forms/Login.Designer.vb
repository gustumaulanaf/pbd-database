﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Login
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.etKdKasirLogin = New System.Windows.Forms.TextBox()
        Me.etPasswordLogin = New System.Windows.Forms.TextBox()
        Me.labelKdKasirLogin = New System.Windows.Forms.Label()
        Me.labelPassword = New System.Windows.Forms.Label()
        Me.OKlogin = New System.Windows.Forms.Button()
        Me.SuspendLayout()
        '
        'etKdKasirLogin
        '
        Me.etKdKasirLogin.Location = New System.Drawing.Point(224, 114)
        Me.etKdKasirLogin.Name = "etKdKasirLogin"
        Me.etKdKasirLogin.Size = New System.Drawing.Size(198, 26)
        Me.etKdKasirLogin.TabIndex = 0
        '
        'etPasswordLogin
        '
        Me.etPasswordLogin.Location = New System.Drawing.Point(224, 180)
        Me.etPasswordLogin.Name = "etPasswordLogin"
        Me.etPasswordLogin.Size = New System.Drawing.Size(198, 26)
        Me.etPasswordLogin.TabIndex = 1
        '
        'labelKdKasirLogin
        '
        Me.labelKdKasirLogin.AutoSize = True
        Me.labelKdKasirLogin.Location = New System.Drawing.Point(99, 120)
        Me.labelKdKasirLogin.Name = "labelKdKasirLogin"
        Me.labelKdKasirLogin.Size = New System.Drawing.Size(85, 20)
        Me.labelKdKasirLogin.TabIndex = 2
        Me.labelKdKasirLogin.Text = "Kode Kasir"
        '
        'labelPassword
        '
        Me.labelPassword.AutoSize = True
        Me.labelPassword.Location = New System.Drawing.Point(99, 186)
        Me.labelPassword.Name = "labelPassword"
        Me.labelPassword.Size = New System.Drawing.Size(78, 20)
        Me.labelPassword.TabIndex = 3
        Me.labelPassword.Text = "Password"
        '
        'OKlogin
        '
        Me.OKlogin.Location = New System.Drawing.Point(288, 235)
        Me.OKlogin.Name = "OKlogin"
        Me.OKlogin.Size = New System.Drawing.Size(75, 31)
        Me.OKlogin.TabIndex = 4
        Me.OKlogin.Text = "OK"
        Me.OKlogin.UseVisualStyleBackColor = True
        '
        'Login
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(507, 291)
        Me.Controls.Add(Me.OKlogin)
        Me.Controls.Add(Me.labelPassword)
        Me.Controls.Add(Me.labelKdKasirLogin)
        Me.Controls.Add(Me.etPasswordLogin)
        Me.Controls.Add(Me.etKdKasirLogin)
        Me.Name = "Login"
        Me.Text = "Login"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents etKdKasirLogin As TextBox
    Friend WithEvents etPasswordLogin As TextBox
    Friend WithEvents labelKdKasirLogin As Label
    Friend WithEvents labelPassword As Label
    Friend WithEvents OKlogin As Button
End Class
