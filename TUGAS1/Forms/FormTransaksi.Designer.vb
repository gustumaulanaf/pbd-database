﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FormTransaksi
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FormTransaksi))
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txtTanggal = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.txtNota = New System.Windows.Forms.TextBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.txtNamaKasir = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.txtKdKasir = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Transaksi = New System.Windows.Forms.GroupBox()
        Me.txtKodeBarang = New System.Windows.Forms.TextBox()
        Me.txtNamaBarang = New System.Windows.Forms.TextBox()
        Me.txtHargaBarang = New System.Windows.Forms.TextBox()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.txtBanyakBarang = New System.Windows.Forms.TextBox()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.btCari = New System.Windows.Forms.Button()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.LabelJumlah = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.LabelTotal = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.txtPotongan = New System.Windows.Forms.TextBox()
        Me.txtBiaya = New System.Windows.Forms.TextBox()
        Me.txtKembali = New System.Windows.Forms.TextBox()
        Me.txtBayar = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.Label15 = New System.Windows.Forms.Label()
        Me.btTambah = New System.Windows.Forms.Button()
        Me.btTutup = New System.Windows.Forms.Button()
        Me.btSimpan = New System.Windows.Forms.Button()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.Transaksi.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.SuspendLayout()
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Perpetua Titling MT", 24.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(104, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(651, 57)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Toko Gustu Maulana F"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txtNota)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txtTanggal)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Location = New System.Drawing.Point(78, 69)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(353, 112)
        Me.GroupBox1.TabIndex = 1
        Me.GroupBox1.TabStop = False
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(3, 27)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(66, 20)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Tanggal"
        '
        'txtTanggal
        '
        Me.txtTanggal.Location = New System.Drawing.Point(121, 27)
        Me.txtTanggal.Name = "txtTanggal"
        Me.txtTanggal.Size = New System.Drawing.Size(198, 26)
        Me.txtTanggal.TabIndex = 1
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(3, 75)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(43, 20)
        Me.Label3.TabIndex = 2
        Me.Label3.Text = "Nota"
        '
        'txtNota
        '
        Me.txtNota.Location = New System.Drawing.Point(121, 72)
        Me.txtNota.Name = "txtNota"
        Me.txtNota.Size = New System.Drawing.Size(198, 26)
        Me.txtNota.TabIndex = 3
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.txtNamaKasir)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Controls.Add(Me.txtKdKasir)
        Me.GroupBox3.Controls.Add(Me.Label5)
        Me.GroupBox3.Location = New System.Drawing.Point(436, 69)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(353, 112)
        Me.GroupBox3.TabIndex = 4
        Me.GroupBox3.TabStop = False
        '
        'txtNamaKasir
        '
        Me.txtNamaKasir.Location = New System.Drawing.Point(121, 69)
        Me.txtNamaKasir.Name = "txtNamaKasir"
        Me.txtNamaKasir.Size = New System.Drawing.Size(198, 26)
        Me.txtNamaKasir.TabIndex = 3
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(3, 72)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(90, 20)
        Me.Label4.TabIndex = 2
        Me.Label4.Text = "Nama Kasir"
        '
        'txtKdKasir
        '
        Me.txtKdKasir.Location = New System.Drawing.Point(121, 27)
        Me.txtKdKasir.Name = "txtKdKasir"
        Me.txtKdKasir.Size = New System.Drawing.Size(198, 26)
        Me.txtKdKasir.TabIndex = 1
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(3, 27)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(44, 20)
        Me.Label5.TabIndex = 0
        Me.Label5.Text = "Kasir"
        '
        'Transaksi
        '
        Me.Transaksi.Controls.Add(Me.LabelTotal)
        Me.Transaksi.Controls.Add(Me.Label11)
        Me.Transaksi.Controls.Add(Me.LabelJumlah)
        Me.Transaksi.Controls.Add(Me.Label10)
        Me.Transaksi.Controls.Add(Me.ListBox1)
        Me.Transaksi.Controls.Add(Me.btCari)
        Me.Transaksi.Controls.Add(Me.Label9)
        Me.Transaksi.Controls.Add(Me.txtBanyakBarang)
        Me.Transaksi.Controls.Add(Me.Label8)
        Me.Transaksi.Controls.Add(Me.Label7)
        Me.Transaksi.Controls.Add(Me.Label6)
        Me.Transaksi.Controls.Add(Me.txtHargaBarang)
        Me.Transaksi.Controls.Add(Me.txtNamaBarang)
        Me.Transaksi.Controls.Add(Me.txtKodeBarang)
        Me.Transaksi.Location = New System.Drawing.Point(78, 187)
        Me.Transaksi.Name = "Transaksi"
        Me.Transaksi.Size = New System.Drawing.Size(711, 274)
        Me.Transaksi.TabIndex = 5
        Me.Transaksi.TabStop = False
        Me.Transaksi.Text = "Transaksi"
        '
        'txtKodeBarang
        '
        Me.txtKodeBarang.Location = New System.Drawing.Point(31, 54)
        Me.txtKodeBarang.Name = "txtKodeBarang"
        Me.txtKodeBarang.Size = New System.Drawing.Size(112, 26)
        Me.txtKodeBarang.TabIndex = 4
        '
        'txtNamaBarang
        '
        Me.txtNamaBarang.Location = New System.Drawing.Point(261, 54)
        Me.txtNamaBarang.Name = "txtNamaBarang"
        Me.txtNamaBarang.Size = New System.Drawing.Size(112, 26)
        Me.txtNamaBarang.TabIndex = 5
        '
        'txtHargaBarang
        '
        Me.txtHargaBarang.Location = New System.Drawing.Point(435, 54)
        Me.txtHargaBarang.Name = "txtHargaBarang"
        Me.txtHargaBarang.Size = New System.Drawing.Size(112, 26)
        Me.txtHargaBarang.TabIndex = 6
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(66, 22)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(46, 20)
        Me.Label6.TabIndex = 4
        Me.Label6.Text = "Kode"
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(295, 22)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(51, 20)
        Me.Label7.TabIndex = 7
        Me.Label7.Text = "Nama"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(470, 22)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(53, 20)
        Me.Label8.TabIndex = 8
        Me.Label8.Text = "Harga"
        '
        'txtBanyakBarang
        '
        Me.txtBanyakBarang.Location = New System.Drawing.Point(592, 54)
        Me.txtBanyakBarang.Name = "txtBanyakBarang"
        Me.txtBanyakBarang.Size = New System.Drawing.Size(112, 26)
        Me.txtBanyakBarang.TabIndex = 9
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(629, 22)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(62, 20)
        Me.Label9.TabIndex = 10
        Me.Label9.Text = "Banyak"
        '
        'btCari
        '
        Me.btCari.Location = New System.Drawing.Point(166, 54)
        Me.btCari.Name = "btCari"
        Me.btCari.Size = New System.Drawing.Size(75, 23)
        Me.btCari.TabIndex = 11
        Me.btCari.Text = "Cari"
        Me.btCari.UseVisualStyleBackColor = True
        '
        'ListBox1
        '
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.ItemHeight = 20
        Me.ListBox1.Location = New System.Drawing.Point(31, 86)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(639, 144)
        Me.ListBox1.TabIndex = 12
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(27, 251)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(116, 20)
        Me.Label10.TabIndex = 13
        Me.Label10.Text = "Jumlah Barang"
        '
        'LabelJumlah
        '
        Me.LabelJumlah.AutoSize = True
        Me.LabelJumlah.Location = New System.Drawing.Point(162, 251)
        Me.LabelJumlah.Name = "LabelJumlah"
        Me.LabelJumlah.Size = New System.Drawing.Size(48, 20)
        Me.LabelJumlah.TabIndex = 14
        Me.LabelJumlah.Text = "Label"
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(431, 242)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(101, 20)
        Me.Label11.TabIndex = 15
        Me.Label11.Text = "Total Belanja"
        '
        'LabelTotal
        '
        Me.LabelTotal.AutoSize = True
        Me.LabelTotal.Location = New System.Drawing.Point(588, 242)
        Me.LabelTotal.Name = "LabelTotal"
        Me.LabelTotal.Size = New System.Drawing.Size(48, 20)
        Me.LabelTotal.TabIndex = 16
        Me.LabelTotal.Text = "Label"
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.btSimpan)
        Me.GroupBox2.Controls.Add(Me.btTutup)
        Me.GroupBox2.Controls.Add(Me.btTambah)
        Me.GroupBox2.Controls.Add(Me.Label15)
        Me.GroupBox2.Controls.Add(Me.Label14)
        Me.GroupBox2.Controls.Add(Me.Label13)
        Me.GroupBox2.Controls.Add(Me.Label12)
        Me.GroupBox2.Controls.Add(Me.txtBayar)
        Me.GroupBox2.Controls.Add(Me.txtKembali)
        Me.GroupBox2.Controls.Add(Me.txtBiaya)
        Me.GroupBox2.Controls.Add(Me.txtPotongan)
        Me.GroupBox2.Location = New System.Drawing.Point(78, 467)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(711, 187)
        Me.GroupBox2.TabIndex = 6
        Me.GroupBox2.TabStop = False
        '
        'txtPotongan
        '
        Me.txtPotongan.Location = New System.Drawing.Point(121, 25)
        Me.txtPotongan.Name = "txtPotongan"
        Me.txtPotongan.Size = New System.Drawing.Size(198, 26)
        Me.txtPotongan.TabIndex = 4
        '
        'txtBiaya
        '
        Me.txtBiaya.Location = New System.Drawing.Point(121, 59)
        Me.txtBiaya.Name = "txtBiaya"
        Me.txtBiaya.Size = New System.Drawing.Size(198, 26)
        Me.txtBiaya.TabIndex = 5
        '
        'txtKembali
        '
        Me.txtKembali.Location = New System.Drawing.Point(121, 129)
        Me.txtKembali.Name = "txtKembali"
        Me.txtKembali.Size = New System.Drawing.Size(198, 26)
        Me.txtKembali.TabIndex = 6
        '
        'txtBayar
        '
        Me.txtBayar.Location = New System.Drawing.Point(121, 95)
        Me.txtBayar.Name = "txtBayar"
        Me.txtBayar.Size = New System.Drawing.Size(198, 26)
        Me.txtBayar.TabIndex = 7
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(32, 31)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(78, 20)
        Me.Label12.TabIndex = 4
        Me.Label12.Text = "Potongan"
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(32, 65)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(87, 20)
        Me.Label13.TabIndex = 8
        Me.Label13.Text = "Total Biaya"
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(32, 101)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(50, 20)
        Me.Label14.TabIndex = 9
        Me.Label14.Text = "Bayar"
        '
        'Label15
        '
        Me.Label15.AutoSize = True
        Me.Label15.Location = New System.Drawing.Point(32, 135)
        Me.Label15.Name = "Label15"
        Me.Label15.Size = New System.Drawing.Size(65, 20)
        Me.Label15.TabIndex = 10
        Me.Label15.Text = "Kembali"
        '
        'btTambah
        '
        Me.btTambah.Image = CType(resources.GetObject("btTambah.Image"), System.Drawing.Image)
        Me.btTambah.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btTambah.Location = New System.Drawing.Point(328, 59)
        Me.btTambah.Name = "btTambah"
        Me.btTambah.Size = New System.Drawing.Size(117, 71)
        Me.btTambah.TabIndex = 17
        Me.btTambah.Text = "Tambah"
        Me.btTambah.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btTambah.UseVisualStyleBackColor = True
        '
        'btTutup
        '
        Me.btTutup.Image = CType(resources.GetObject("btTutup.Image"), System.Drawing.Image)
        Me.btTutup.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btTutup.Location = New System.Drawing.Point(580, 59)
        Me.btTutup.Name = "btTutup"
        Me.btTutup.Size = New System.Drawing.Size(117, 71)
        Me.btTutup.TabIndex = 18
        Me.btTutup.Text = "Tutup"
        Me.btTutup.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btTutup.UseVisualStyleBackColor = True
        '
        'btSimpan
        '
        Me.btSimpan.Image = CType(resources.GetObject("btSimpan.Image"), System.Drawing.Image)
        Me.btSimpan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft
        Me.btSimpan.Location = New System.Drawing.Point(454, 59)
        Me.btSimpan.Name = "btSimpan"
        Me.btSimpan.Size = New System.Drawing.Size(117, 71)
        Me.btSimpan.TabIndex = 19
        Me.btSimpan.Text = "Simpan"
        Me.btSimpan.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btSimpan.UseVisualStyleBackColor = True
        '
        'FormTransaksi
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(9.0!, 20.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(871, 666)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.Transaksi)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Label1)
        Me.Name = "FormTransaksi"
        Me.Text = "FormTransaksi"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.Transaksi.ResumeLayout(False)
        Me.Transaksi.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents Label1 As Label
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents txtNota As TextBox
    Friend WithEvents Label3 As Label
    Friend WithEvents txtTanggal As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents txtNamaKasir As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents txtKdKasir As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents Transaksi As GroupBox
    Friend WithEvents ListBox1 As ListBox
    Friend WithEvents btCari As Button
    Friend WithEvents Label9 As Label
    Friend WithEvents txtBanyakBarang As TextBox
    Friend WithEvents Label8 As Label
    Friend WithEvents Label7 As Label
    Friend WithEvents Label6 As Label
    Friend WithEvents txtHargaBarang As TextBox
    Friend WithEvents txtNamaBarang As TextBox
    Friend WithEvents txtKodeBarang As TextBox
    Friend WithEvents LabelTotal As Label
    Friend WithEvents Label11 As Label
    Friend WithEvents LabelJumlah As Label
    Friend WithEvents Label10 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents btSimpan As Button
    Friend WithEvents btTutup As Button
    Friend WithEvents btTambah As Button
    Friend WithEvents Label15 As Label
    Friend WithEvents Label14 As Label
    Friend WithEvents Label13 As Label
    Friend WithEvents Label12 As Label
    Friend WithEvents txtBayar As TextBox
    Friend WithEvents txtKembali As TextBox
    Friend WithEvents txtBiaya As TextBox
    Friend WithEvents txtPotongan As TextBox
End Class
